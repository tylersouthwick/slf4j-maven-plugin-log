/*
 * This file is part of slf4j-maven-plugin-log.
 *
 * slf4j-maven-plugin-log is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * slf4j-maven-plugin-log is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with slf4j-maven-plugin-log.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.slf4j.impl;

import org.apache.maven.plugin.logging.Log;
import org.slf4j.Logger;

/**
 * Simple {@link Log} adapter to {@link Logger}.
 * 
 * @author Fran�ois Lecomte
 */
public class MavenLogAdapter extends MarkerIgnoringBase implements Logger {

    /**
     * Maven {@link Log} delegate
     */
    private Log log;

    /**
     * Constructor
     * 
     * @param log Maven {@link Log} delegate
     */
    MavenLogAdapter(Log log) {
        super();
        this.log = log;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return log.getClass().getName();
    }

    // debug

    /**
     * {@inheritDoc}
     */
    public boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public void debug(String format, Object arg1, Object arg2) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void debug(String format, Object arg) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void debug(String format, Object[] argArray) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void debug(String msg, Throwable t) {
        log.debug(msg, t);
    }

    /**
     * {@inheritDoc}
     */
    public void debug(String msg) {
        log.debug(msg);
    }

    // error

    /**
     * {@inheritDoc}
     */
    public boolean isErrorEnabled() {
        return log.isErrorEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public void error(String format, Object arg1, Object arg2) {
        if (log.isErrorEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            log.error(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void error(String format, Object arg) {
        if (log.isErrorEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            log.error(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void error(String format, Object[] argArray) {
        if (log.isErrorEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            log.error(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void error(String msg, Throwable t) {
        log.error(msg, t);
    }

    /**
     * {@inheritDoc}
     */
    public void error(String msg) {
        log.error(msg);
    }

    // info

    /**
     * {@inheritDoc}
     */
    public boolean isInfoEnabled() {
        return log.isInfoEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public void info(String format, Object arg1, Object arg2) {
        if (log.isInfoEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            log.info(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void info(String format, Object arg) {
        if (log.isInfoEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            log.info(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void info(String format, Object[] argArray) {
        if (log.isInfoEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            log.info(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void info(String msg, Throwable t) {
        log.info(msg, t);
    }

    /**
     * {@inheritDoc}
     */
    public void info(String msg) {
        log.info(msg);
    }

    // trace

    /**
     * {@inheritDoc}
     */
    public boolean isTraceEnabled() {
        return log.isDebugEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public void trace(String format, Object arg1, Object arg2) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void trace(String format, Object arg) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void trace(String format, Object[] argArray) {
        if (log.isDebugEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            log.debug(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void trace(String msg, Throwable t) {
        log.debug(msg, t);
    }

    /**
     * {@inheritDoc}
     */
    public void trace(String msg) {
        log.debug(msg);
    }

    // warn

    /**
     * {@inheritDoc}
     */
    public boolean isWarnEnabled() {
        return log.isWarnEnabled();
    }

    /**
     * {@inheritDoc}
     */
    public void warn(String format, Object arg1, Object arg2) {
        if (log.isWarnEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg1, arg2);
            log.warn(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void warn(String format, Object arg) {
        if (log.isWarnEnabled()) {
            final String msgStr = MessageFormatter.format(format, arg);
            log.warn(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void warn(String format, Object[] argArray) {
        if (log.isWarnEnabled()) {
            final String msgStr = MessageFormatter.arrayFormat(format, argArray);
            log.warn(msgStr);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void warn(String msg, Throwable t) {
        log.warn(msg, t);
    }

    /**
     * {@inheritDoc}
     */
    public void warn(String msg) {
        log.warn(msg);
    }

}
